# Pre-Requisites

## So, what do I need?

### GitLab.com Account

To make this work you'll need a gitlab.com account, thats what I'm using to create this site. I guess this is possible to do on github.com too but I've not tries that, we use gitlab at work so it made sense that I used that in my demo/PoC

Getting a gitlab.com account is easy, just go along to [GitLab](https://gitlab.com) and sign up. This is all possible with a free account. No needs to pay!

### Anything Else?

Well... erm... no, that's it really. Everything else is taken care of inside GitLab, so let's move on to the configuration step.