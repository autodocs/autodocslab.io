# Configuration

So, you've got your gitlab.com account and you're eager to start.  

Gitlab Pages allow for you to create a site based either on your username or a group name within gitlab. So what does this mean?  

Well, if your gitlab username is dave, then you can set up you pages as dave.gitlab.io so long as it's not already taken.  

Alternatively, you can create yourself a group within GitLab and use that as the site name, that's what I did in this instance. Under my gitlab.com username, I created a group called autodocs. So, personally, I'd suggest you think about what you want your site to be called and check that it isn't already taken.

## OK, I've got my name, what next?

Next you need to create a project, either within your personal project space (if you're using your username for the site name) or under the group you created (if you want the group name to be the site name). For the remainder of this guide I'm going to assume you chose to do it under the group name.

### So what should my project be called?

Well this one is easy, if your group is called autodocs (like mine is), you create yourself a project called autodocs.gitlab.io

### Done that, what's next?

Once you've got your project created the first thing I did was created a .gitlab-ci.yml file. This file is essentially a config file that tells gitlab to execute the mkdocs workflow to generate your site based on the markdown files that you have (not yet) created. It uses the CI/CD pipelines to do this. Here's the file I used and I'll explain what each bit


```
image: python:3.8-buster

before_script:
  - pip install mkdocs
  - pip install mkdocs-material
  
pages:
  script:
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
  - master
```

Break it down now...

```
image: python:3.8-buster
```
This tells the CI/CD pipeline to use a python version 3.8 buster image to compile the site

```
before_script:
  - pip install mkdocs
  - pip install mkdocs-material
```
This executes some little bits before the main part of the script, in this case it tells the CI/CD runner to install mkdocs (which is used to generate the site) and mkdocs-material (which is the theme I have opted to use for my site, as material isn't a default theme I've had to install it first)

```
pages:
  script:
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
  - master
```
This is the main part of the workload defined under the **pages** section and it does a few things:
#### script
There are two parts to this script, the first command tells mkdocs to build the site (I bet you could have guessed that) and the second command moves the generated site into a directory called public (again, pretty easy to work out by looking at the command)

#### artifacts
By default anything that runs in the CI/CD runner is transient and without specifing **artifacts** they would just be deleted when the job finishes. When they are specified like this these files are uploaded back to GitLab. In this instance we are only copying the files in the public directory back to GitLab

#### only
This is a simple section which is basically saying that it's only going to execute this on a commit to the **master** branch, this will be explained a bit more in an upcoming section.

