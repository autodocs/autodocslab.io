# Mark's Lockdown Brownies

So, I made some brownies whilst in lockdown and I was asked to share the recipe. I'm by no means a great baker but they do turn out nice everytime I've made them.

## Ingredients

- 275g of softened butter (they say to use unsalted butter but I quite like it a bit salty, so it's up to you)
- 375g of caster sugar (the last time I made these I only had 300g of caster sugar so used 75g of normal granualted white sugar)
- 25g of cocoa powder
- 100g of self raising flour
- 4 large eggs
- 125g of dark chocolate
- 200g of white chocolate (I used a couple of 100g bars of Milkybar)

## Pre-Method

1. Pre-heat oven to about 170c (we have a fan over so adjust as needed).
2. Line a baking tin/tray with baking paper and grease. Mine was about 30cm x 20cm

## Method

1. Added the **sugar**, **cocoa powder** and **flour** into a mixing bowl.
2. Chopped up the **butter** (as much as you can chop softened butter) into smaller peices and also put in the mixing bowl.
3. Turned on the mixer to combine these ingredients (until you end up with a chocolately brown paste).
4. Chopped the **dark chocolate** and melted over a pan of simmering water.
5. Add melted chocolate to mixer and combine with sugary, floury chocolate paste until it becomes even more chocolately and pastey.
6. Beat together the 4 **eggs**.
7. Add to the mixer and continue to mix together until contents looks like a brown batter. Avoid tempation to eat a spoonful, unless you like raw eggs.
8. Chop **white chocolate** into really small chunks, or big chunks of you're into that. Depends how big you like your chunks.
9. Add chunks to mixer and mix a bit more until they're well distributed into batter.
10. Tip the chocolately batter into the pre-greased baking tray. Spread it out evenly in the tray of needed
11. Stick it in the oven for about 50 minutes and then use a skewer to poke it to make sure it comes out clean and not still covered in raw batter.
12. If the top looks like it's burning loosely cover it with some foil.
13. Take out of oven and leave for about an hour (yes I know, how long!?) They will continue to cook during that time.
14. Cut into about 24 small pieces, or make them bigger if you like. I'm not your boss, do what you want.


## Alternatives

If you don't have any white chocolate, or don't like white chocolate (what the hell is wrong with you?) then try other things instead, I recommend the following:
- Oreos  
- Cadbury Caramel Buttons  
- Cadbury Crunchie  
- Some kind of nuts maybe  
- Marshmellows, not sure if they'd melt though  
- Toblerone, the small ones though not them big ones you get from Duty Free.  

To be fair you can probably use anything, I haven't tried half of these things but I'd quite like to.

Randomly, one thing I have tried was a Terry's Chocolate Orange and although it tested nice the chocolate went a bit weird, I think it's to do with the orange oil they use to flavour it. Try it if you like though and let me know how it goes for you, maybe I messed up.

Anyway, thats basically it.
