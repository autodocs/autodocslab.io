# Automated Documentation PoC

## What's it all about?

Do you work somewhere where your documentation is stuck in the dark ages? A massive amount of Microsoft Word files locked away in convoluted SharePoint spaces or on network shares?  
Are you wondering if there is a better way to have your documentation that looks like the fancy-pants lot over at Google Cloud, Azure, AWS or even here at GitLab?  

Well, the answer it seems is "Yes, there is a better way". Many of you probably already know about it but I am very slow at joining the party, so this site is here as a test-bed, proof-of-concept, sandpit play area for me to show off to people I work with as well as that feeling of pride when you do something at it works (even if it's not the most exciting or cutting edge). If it turns out that it's useful to someone else then brilliant, knock yourself out with it. 

This site is build by using a combination of GitLab, Markdown, MKDocs and Gitlab pages for hosting.

The idea is that I write my documentation in my Gitlab Repo using markdown, then using CI pipelines it calls a docker image running python and installs MKDocs and compiles a website based on my markdown files, then publishes them to Gitlab Pages.

## Why am I doing this?

This is being used as a test case to write and host documentation interally at work using our local gitlab instance instead of using pre-templated Word documents. At present this gitlab.com instance is my basic proof of concept, I will be using this as my starting point for hosting it internally.

## What else?

I'm also using this as a way of learning about git as well as markdown. So hopefully, as I get better at it these pages will start to look better :D
